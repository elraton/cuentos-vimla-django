from django.contrib import admin

from web.models import History


class HistoryAdmin(admin.ModelAdmin):
    list_display = ('id', 'history', 'show_at_home', 'ip', 'created_at', )
    list_display_links = list_display
    list_filter = ('created_at', 'show_at_home')


admin.site.register(History, HistoryAdmin)
