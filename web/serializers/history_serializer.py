from rest_framework.serializers import ModelSerializer, SerializerMethodField

from web.models import History


class HistorySerializer(ModelSerializer):

    class Meta:
        model = History
        exclude = ('created_at', 'ip')
