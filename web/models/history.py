from django.db.models import Model, TextField, BooleanField, DateField, CharField


class History(Model):
    history = TextField()
    show_at_home = BooleanField(default=False)
    created_at = DateField(auto_created=True)
    ip = CharField(max_length=50, null=True)
