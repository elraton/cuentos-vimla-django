from rest_framework.routers import SimpleRouter

from web.views import Home, HistoryViewSet
from django.urls import path, include

router = SimpleRouter()
router.register('history', HistoryViewSet, basename='history')

urlpatterns = [
    path('', Home.as_view(), name='index'),
    path('api/', include(router.urls))
]
