from django.shortcuts import render
from django.views import View

from web.models import History


class Home(View):

    def get(self, request):
        history = History.objects.filter(show_at_home=True)
        return render(request, 'home.html', {'histories': history})

