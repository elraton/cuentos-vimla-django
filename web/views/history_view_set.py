import datetime
from rest_framework.mixins import CreateModelMixin
from rest_framework.request import Request
from rest_framework.viewsets import GenericViewSet
from rest_framework.permissions import BasePermission, SAFE_METHODS

from web.models import History
from web.serializers import HistorySerializer


def visitor_ip_address(request):

    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')

    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip


class HistoryPerDayPermission(BasePermission):
    def has_permission(self, request: Request, view) -> bool:
        client_ip = visitor_ip_address(request)
        current_history = History.objects.filter(ip=client_ip, created_at=datetime.datetime.now().date())
        if len(current_history) > 0:
            return False
        else:
            return True


class HistoryViewSet(GenericViewSet, CreateModelMixin):
    permission_classes = (HistoryPerDayPermission,)
    serializer_class = HistorySerializer

    def get_queryset(self):
        client_ip = visitor_ip_address(self.request)
        return History.objects.filter(ip=client_ip, created_at=datetime.datetime.now().date())

    def perform_create(self, serializer):
        client_ip = visitor_ip_address(self.request)
        serializer.validated_data['ip'] = client_ip
        serializer.validated_data['created_at'] = datetime.datetime.now().date()
        serializer.is_valid(raise_exception=True)
        serializer.save()
