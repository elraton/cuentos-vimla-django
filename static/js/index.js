var countDownDate = new Date("Jan 5, 2021 15:37:25").getTime();
var x = setInterval(function() {

    var now = new Date().getTime();
  
    var distance = countDownDate - now;
  
    var days = Math.floor(distance / (1000 * 60 * 60 * 24));
    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    var seconds = Math.floor((distance % (1000 * 60)) / 1000);

    const days1 = days.toString()[0];
    const days2 = days < 10 ? '0' : days.toString()[1];
    document.querySelector('#day1').innerHTML = days1;
    document.querySelector('#day2').innerHTML = days2;

    const hours1 = hours.toString()[0];
    const hours2 = hours < 10 ? '0' : hours.toString()[1];
    document.querySelector('#hour1').innerHTML = hours1;
    document.querySelector('#hour2').innerHTML = hours2;
    
    const minutes1 = minutes.toString()[0];
    const minutes2 = minutes < 10 ? '0' : minutes.toString()[1];
    document.querySelector('#minute1').innerHTML = minutes2;
    document.querySelector('#minute2').innerHTML = minutes1;
    
    const seconds1 = seconds.toString()[0];
    const seconds2 = seconds < 10 ? '0' : seconds.toString()[1];
    document.querySelector('#second1').innerHTML = seconds1;
    document.querySelector('#second2').innerHTML = seconds2;
  
    if (distance < 0) {
      clearInterval(x);
      document.getElementById("demo").innerHTML = "EXPIRED";
    }
  }, 1000);