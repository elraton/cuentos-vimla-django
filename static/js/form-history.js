toastr.options.positionClass = 'toast-top-right';
toastr.options.extendedTimeOut = 0; //1000;
toastr.options.timeOut = 1000;
toastr.options.fadeOut = 250;
toastr.options.fadeIn = 250;

const historyButton = document.querySelector('#send-history');

function processForm() {
    const historyText = document.querySelector('#history-content');
    const token = document.querySelector('input[name=csrfmiddlewaretoken]');
    const url = '/api/history/';
    var data = {
        history: historyText.value,
        csrfmiddlewaretoken: token.value
    };

    fetch(url, {
        method: 'POST',
        body: JSON.stringify(data),
        headers:{
            'Content-Type': 'application/json',
            'X-CSRFToken': token.value
        }
    }).then(res => {
        console.log(res);
        console.log(res.status);
        if (res.status === 200 || res.status === 201) {
            toastr.success('Historia registrada', '')
            console.log('response success');
        } else {
            toastr.error('Ocurrio un error', '')
            console.log('response error');
        }
    })
}

if (historyButton) {
    historyButton.onclick = processForm;
}